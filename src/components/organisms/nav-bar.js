import React from "react";
import Link from "next/link";
import { motion } from "framer-motion";
import Actions from "../molecules/actions";
import { H6 } from "../atoms/typography";

function Navbar() {
  return (
    <motion.nav className="md:flex hidden justify-between items-center gap-5 text-base py-3 flex-col md:flex-row">
      <Link href={"/"} passHref={true}>
        <a>
          <H6
            text="JHUSSAIN.COM"
            className="cursor-pointer"
            animate={{ x: [-500, 0] }}
            transition={{ type: "spring", duration: 1 }}
            whileHover={{ scale: 1.2 }}
            whileTap={{ scale: 0.8 }}
          />
        </a>
      </Link>

      <Actions />
    </motion.nav>
  );
}

export default Navbar;
