import React from "react";

import { skillsData } from "../../constant";
import SkillBox from "../molecules/skill-box";

const Skills = () => {
  return (
    <div className="flex md:flex-row flex-col justify-center items-center m-auto gap-5 bg-grey-100 p-5 overflow-hidden">
      <SkillBox
        data={skillsData.slice(0, 1)}
        titleClassName="text-3xl text-black"
        animate="left"
        imageData
      />
      <SkillBox data={skillsData.slice(1)} animate="right" />
    </div>
  );
};

export default Skills;
