import React, { useEffect } from "react";
import Image from "next/image";
import classNames from "classnames";
import { useInView } from "react-intersection-observer";
import { motion, useAnimation } from "framer-motion";
import { LinedButton } from "../atoms/button";

const variants = {
  hidden_left: (i) => ({ opacity: 0, x: -100 }),
  hidden_right: (i) => ({ opacity: 0, x: 100 }),
  right: (i) => ({
    opacity: [0, 1],
    x: [100, 100, 0, 0],
    transition: {
      delay: (i + 1) * 0.3,
      duration: 1,
    },
  }),
  left: (i) => ({
    opacity: [0, 1],
    x: [-100, -100, 0, 0],
    transition: {
      delay: (i + 1) * 0.3,
      duration: 1,
    },
  }),
};

const EachSkillBox = ({ titleClassName, animate, title, items, i }) => {
  const controls = useAnimation();
  const [ref, inView] = useInView();

  useEffect(() => {
    if (inView) {
      controls.start(animate);
    } else {
      controls.start(`hidden_${animate}`);
    }
  }, [animate, controls, inView]);
  return (
    <motion.div
      className=" h-full flex flex-col justify-center w-full"
      custom={i}
      animate={controls}
      initial={`hidden_${animate}`}
      variants={variants}
      ref={ref}
    >
      <div className="w-full p-2 text-left border-black-100">
        <spam
          className={classNames(
            "uppercase font-bold",
            { "text-black-300 text-lg": titleClassName === "" },
            { [titleClassName]: titleClassName !== "" }
          )}
        >
          {title}
        </spam>
      </div>
      <div className="flex flex-wrap gap-2 items-center p-2 justify-start">
        {items.map((f, j) => (
          <LinedButton key={j} text={f} />
        ))}
      </div>
    </motion.div>
  );
};

const SkillBox = ({
  data,
  titleClassName = "",
  animate = "right",
  imageData,
}) => {
  const controls = useAnimation();
  const [ref, inView] = useInView();

  useEffect(() => {
    if (inView) {
      controls.start(animate);
    } else {
      controls.start(`hidden_${animate}`);
    }
  }, [animate, controls, inView]);
  return (
    <motion.div className="flex flex-col gap-5 justify-center items-start max-w-xl">
      {data.map((e, i) => (
        <EachSkillBox
          key={i}
          titleClassName={titleClassName}
          animate={animate}
          title={e.title}
          items={e.items}
          i={i}
        />
      ))}

      {imageData && (
        <motion.div
          className="md:flex justify-center max-w-xs md:max-w-lg items-center flex-col"
          custom={data.length}
          animate={controls}
          initial="hidden"
          variants={variants}
          ref={ref}
        >
          <Image
            src={"/illustrations/version_control.svg"}
            width={400}
            height={400}
            alt="version_control"
          />
        </motion.div>
      )}
    </motion.div>
  );
};

export default SkillBox;
