import React from "react";
import Image from "next/image";
import { motion } from "framer-motion";

const Programmer = () => {
  return (
    <motion.div
      className="md:flex justify-center max-w-xs md:max-w-lg"
      animate={{ x: [100, 0] }}
      transition={{ type: "spring", duration: 2 }}
    >
      <motion.div className="absolute max-w-xs md:max-w-lg">
        <motion.div className="relative">
          <motion.div
            className="absolute bubble"
            animate={{ rotate: 360, x: [-800, 0] }}
            transition={{ duration: 2, type: "tween" }}
          >
            <Image
              src={"/illustrations/programming-bg.svg"}
              width={600}
              height={600}
              alt="programming"
            />
          </motion.div>
          <motion.div
            animate={{ rotate: 360, x: [-800, 0] }}
            transition={{ duration: 2, type: "tween" }}
          >
            <Image
              src={"/illustrations/programming-bg-circle.svg"}
              width={600}
              height={600}
              alt="programming"
            />
          </motion.div>
        </motion.div>
      </motion.div>
      <Image
        src={"/illustrations/programming.svg"}
        width={600}
        height={600}
        alt="programming"
      />
    </motion.div>
  );
};

export default Programmer;
